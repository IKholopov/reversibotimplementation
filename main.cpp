#include <StreamBot.h>
#include <MiniMaxBot.h>

#include <iostream>
#include <memory>

int main(int argc, char *argv[])
{
    StreamBot bot(std::unique_ptr<IBot>(new MiniMaxBot()), std::cin, std::cout);
    bot.Run();
    return 0;
}

